package com.sapient.ams.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sapient.ams.entitys.Airport;

public interface IAirportsDAO extends JpaRepository<Airport, Long> {

	public List<Airport> findByNameLike(String name);

	public List<Airport> findByType(String type);

	public List<Airport> findByLatitude(String latitude);

	public List<Airport> findByLongitude(String longitude);

	
}
