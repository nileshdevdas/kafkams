package com.sapient.ams.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sapient.ams.entitys.User;

public interface UserDAO extends JpaRepository<User, Long> {

}
