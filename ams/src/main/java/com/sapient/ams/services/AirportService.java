package com.sapient.ams.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sapient.ams.dao.IAirportsDAO;
import com.sapient.ams.entitys.Airport;

@Service
public class AirportService {

	@Autowired
	private IAirportsDAO dao;

	public List<Airport> getAllAirports() {
		return dao.findAll();
	}

	public List<Airport> getByName(String name) {
		return dao.findByNameLike(name);
	}

	public List<Airport> getByType(String type) {
		return dao.findByType(type);
	}

}
