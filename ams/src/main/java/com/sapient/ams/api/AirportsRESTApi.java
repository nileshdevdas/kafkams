package com.sapient.ams.api;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.ams.entitys.Airport;
import com.sapient.ams.services.AirportService;

@RestController
public class AirportsRESTApi {

	@Autowired
	private AirportService service;

	@GetMapping(path = "all", produces = "application/json")
	public List<Airport> listAll() {
		System.out.println("Listing.........................");
		return service.getAllAirports();
	}

	@GetMapping(path = "byname/{name}", produces = "application/json")
	public List<Airport> findByName(@PathVariable("name") String name) {
		System.out.println(name);
		System.out.println("Listing.........................");
		return service.getByName(name);
	}

	@GetMapping(path = "bytype/{type}", produces = "application/json")
	public List<Airport> findByType(@PathVariable("type") String type) {
		System.out.println(type);
		System.out.println("Listing.........................");
		return service.getByType(type);
	}

}
