package com.sapient.ams.security;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.ams.entitys.User;

@RestController
public class LoginService {

	@Autowired
	private JwtUtils jwtUtils;

	@PostMapping(path = "/api/auth/signin", produces = "application/json", consumes = "application/json")
	public User login(@RequestBody User user) {
		user.setToken(jwtUtils.generateJwtToken(user.getUsername(), user.getPassword()));
		user.setPassword("");
		return user;
	}
}
