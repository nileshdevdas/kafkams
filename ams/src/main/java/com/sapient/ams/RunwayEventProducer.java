package com.sapient.ams;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

/**
 * A Service that will get called when ever a plane lands ....... Who willl who
 * ever wishes to integrated the landing....
 * 
 * @author niles
 *
 */

@Service
public class RunwayEventProducer {

	@Autowired
	private KafkaTemplate<String, String> kTemplate;

	/** every 5 Secs i expect a flight to land */
	@Scheduled(fixedRate = 5000)
	public void sendMessages() {
		kTemplate.send("RUNWAY", "{\"id\" : \""+  UUID.randomUUID() + "\" , \"action\" : \"Landed\"}" );
		System.out.println("Flight Landed....");
	}
}
