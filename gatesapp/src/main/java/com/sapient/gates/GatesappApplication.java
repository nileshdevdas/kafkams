package com.sapient.gates;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GatesappApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatesappApplication.class, args);
	}

}
