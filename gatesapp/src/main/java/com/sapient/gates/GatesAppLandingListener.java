package com.sapient.gates;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class GatesAppLandingListener {

	@KafkaListener(topics = "RUNWAY")
	public void listenForLandings(String message) {
		System.out.println("GATES.............................GATES");
		System.out.println(message);
		System.out.println("GATES.............................GATES");

	}
}
