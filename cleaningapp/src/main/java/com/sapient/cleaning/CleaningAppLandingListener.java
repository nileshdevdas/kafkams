package com.sapient.cleaning;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class CleaningAppLandingListener {

	@KafkaListener(topics = "RUNWAY")
	public void listenForLandings(String message) {
		System.out.println(".............................");
		System.out.println(message);
		System.out.println(".............................");

	}
}
