package com.sapient.cleaning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PantryappApplication {

	public static void main(String[] args) {
		SpringApplication.run(PantryappApplication.class, args);
	}

}
